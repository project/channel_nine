<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

  <head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>

<body>

  <?php $mission = theme_get_setting('mission', false); ?>
  <?php if ($mission || $left) { ?>
    <div id="left">
      <?php if ($mission != ""): ?>
        <div id="mission"> 
        <h1>Our Mission</h1>
        <p>
        <?php print $mission; ?>
        </p>
        </div> 
      <?php endif; ?> 
      <?php if ($left != ""): ?>
        <?php print $left ?> <!-- print left sidebar if any blocks enabled -->
      <?php endif; ?> 
    </div>
  <?php }; ?>

  <div id="center" style="width: <?php print channel_nine_get_center_width($left, $right1, $right2) ?>%;">
    <div class="banner">
      
    </div>
    <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
      <?php print $site_name ?></a>
    </h1><?php } ?>
    <?php if ($site_slogan) { ?><div id="slogan"><?php print $site_slogan ?></div><?php } ?>	 

    <div id="breadcrumb">
     <?php print $breadcrumb ?>
    </div>

    <?php print $help ?>
    <?php print $messages ?>

    <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>	 
    <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>	 

    <?php print $content; ?>

    <div id="footer">
      <?php print $footer_message ?>
      <?php print $closure ?>
    </div>
  </div>

  <?php if ($right1) { ?>
    <div id="right1">
      <?php print $right1 ?>
    </div>
  <?php } ?>

  <?php if ($right2) { ?>
    <div id="right2">
      <?php print $right2 ?>
    </div>
  <?php } ?>

</body>
</html>

