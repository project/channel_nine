<?php
function channel_nine_regions() {
  return array(
       'left' => t('left sidebar'),
       'right1' => t('right sidebar1'),
       'right2' => t('right sidebar2'),
       'content' => t('content'),
       'header' => t('header'),
       'footer' => t('footer')
  );
}
function channel_nine_get_center_width($left, $right1, $right2) {
  $width = 44;
  if (!$left) {
    $width = $width + 18;
  }  
  if (!$right1) {
    $width = $width + 19;
  }  
  if (!$right2) {
    $width = $width + 14;
  }  
  return $width;
}

function phptemplate_system_themes($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = drupal_render($form[$key]['description']) . drupal_render($form[$key]['screenshot']) ;
      $row[] = array('data' => drupal_render($form['status'][$key]), 'align' => 'center');
      if ($form['theme_default']) {
        $row[] = array('data' => drupal_render($form['theme_default'][$key]) . drupal_render($form[$key]['operations']), 'align' => 'center');
      }
    }
    $rows[] = $row;
  }
  $header = array(t('Name/Screenshot'), t('Enabled'), t('Default'));
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}
function phptemplate_system_user($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = drupal_render($form[$key]['description']) . drupal_render($form[$key]['screenshot']);
      $row[] = array('data' => drupal_render($form['theme'][$key]), 'align' => 'center');
    }
    $rows[] = $row;
  }

  $header = array(t('Name/Screenshot'), t('Selected'));
  $output = theme('table', $header, $rows);
  return $output;
}
?>
